// ObjectOrientatedMouse.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"
using namespace std;
#define X_MAX 10
#define Y_MAX 10

//無痛 12-15

class CMouse {
private:
	int ix, iy;
	int iStatus;
	char cIcon;
public:
	void init() {
		ix = 1;
		iy = 1;
		iStatus = 1;
		cIcon = '@';
	}
	void Show();
	int Update(char cIn);
	int GetStatus() { return iStatus; }; //想用私有變數只能用公用函式存取
};

int CMouse::Update(char cIn) {
	switch (cIn) {
	case 'w':
		ix--;
		if (ix < 0) {
			iStatus = 0;
		}
		else if (ix == 0) {
			iStatus = 2;
			cIcon = 'Q';
		}
		else if (iy >= 1 && iy <= Y_MAX) {
			iStatus = 1;
			cIcon = '@';
		}
		break;
	case 's':
		ix++;
		if (ix > X_MAX + 1) {
			iStatus = 0;
		}
		else if (ix == X_MAX + 1) {
			iStatus = 2;
			cIcon = 'Q';
		}
		else if (iy >= 1 && iy <= Y_MAX) {
			iStatus = 1;
			cIcon = '@';
		}
		break;
	case 'a':
		iy--;
		if (iy < 0) {
			iStatus = 0;
		}
		else if (iy == 0) {
			iStatus = 2;
			cIcon = 'Q';
		}
		else if (ix >= 1 && ix <= X_MAX) {
			iStatus = 1;
			cIcon = '@';
		}
		break;
	case 'd':
		iy++;
		if (iy > Y_MAX + 1) {
			iStatus = 0;
		}
		else if (iy == Y_MAX + 1) {
			iStatus = 2;
			cIcon = 'Q';
		}
		else if (ix >= 1 && ix <= X_MAX) {
			iStatus = 1;
			cIcon = '@';
		}
		break;
	}
	return iStatus;
}

void CMouse::Show() {
	system("cls");
	for (int i = 0;i < ix;i++) cout << endl;
	cout << setw(iy + 1) << setfill(' ') << cIcon << endl;
}

int main()
{
	char cIn;
	int iStatus;
	CMouse mouseX;
	mouseX.init();
	mouseX.Show();
	//iStatus = mouseX.iStatus; 不能直接存取
	iStatus = mouseX.GetStatus();
	while (iStatus) {
		cIn = _getch();
		iStatus = mouseX.Update(cIn);
		if (iStatus) mouseX.Show();
		else cout << "結束遊戲" << endl;
	}
	system("pause");
	return 0;
}

