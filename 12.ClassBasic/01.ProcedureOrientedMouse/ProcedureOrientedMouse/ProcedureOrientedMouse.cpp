// ProcedureOrientedMouse.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"
using namespace std;

//無痛 12-4

#define X_MAX 10
#define Y_MAX 10
void ShowMouse();
int ix = 1, iy = 1;
int iStatus = 1;
char cIcon = '@';

int main()
{
	char cIn;
	ShowMouse();
	while (iStatus) {
		cIn = _getch();
		switch (cIn) {
			case 'w':{
				ix--;
				if (ix < 0) iStatus = 0; //老鼠死亡
				else if (ix == 0) {
					iStatus = 2;
					cIcon = 'Q';
				}
				else if (iy > 0 && iy <= Y_MAX) {
					iStatus = 1;
					cIcon = '@';
				}
				break;
			}
			case 'a': {
				iy--;
				if (iy < 0) iStatus = 0; //老鼠死亡
				else if (iy == 0) {
					iStatus = 2;
					cIcon = 'Q';
				}
				else if (ix > 0 && ix <= Y_MAX) {
					iStatus = 1;
					cIcon = '@';
				}
				break;
			}
			case 's': {
				ix++;
				if (ix > Y_MAX + 1) iStatus = 0; //老鼠死亡
				else if (ix == Y_MAX + 1) {
					iStatus = 2;
					cIcon = 'Q';
				}
				else if (iy > 0 && iy <= Y_MAX) {
					iStatus = 1;
					cIcon = '@';
				}
				break;
			}
			case 'd': {
				iy++;
				if (iy > Y_MAX + 1) iStatus = 0; //老鼠死亡
				else if (iy == Y_MAX + 1) {
					iStatus = 2;
					cIcon = 'Q';
				}
				else if (ix > 0 && ix <= Y_MAX) {
					iStatus = 1;
					cIcon = '@';
				}
				break;
			}
		}
		if (iStatus) ShowMouse();
		else cout << "遊戲結束!" << endl;
	}
	system("pause");
    return 0;
}

void ShowMouse() {
	system("cls");
	for (int i = 0;i < ix;i++) cout << endl;
	cout << setw(iy+1) << setfill(' ') << cIcon << endl;
}
