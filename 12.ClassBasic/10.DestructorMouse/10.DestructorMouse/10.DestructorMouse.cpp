// 10.DestructorMouse.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"
using namespace std;
#define X_MAX 10
#define Y_MAX 10

//無痛 12-32

class CMouse {
private:
	int ix, iy;
	char cIcon;
	int iStatus;
	char *cMicon;
public:
	CMouse() {
		ix = iy = 1;
		cMicon = new char[2];
		iStatus = 1;
		cIcon = cMicon[0] = '@';
		cMicon[1] = 'Q';
	}
	~CMouse() {
		delete[] cMicon;
	}
	void Show();
	void Update(char cIn);
	int GetStatus() { return iStatus; };
};

void CMouse::Show() {
	system("cls");
	for (int i = 0;i < ix;i++)cout << endl;
	cout << setw(iy + 1) << setfill(' ') << cIcon << endl;
}

void CMouse::Update(char cIn) {
	switch (cIn) {
		case 'w':
			ix--;
			if (ix < 0) {
				iStatus = 0;
			}
			else if (ix == 0) {
				iStatus = 2;
				cIcon = cMicon[1];
			}
			else if (iy > 0 && iy <= Y_MAX) {
				iStatus = 1;
				cIcon = cMicon[0];
			}
			break;
		case 's':
			ix++;
			if (ix > X_MAX + 1) {
				iStatus = 0;
			}
			else if (ix == X_MAX + 1) {
				iStatus = 2;
				cIcon = cMicon[1];
			}
			else if (iy > 0 && iy <= Y_MAX) {
				iStatus = 1;
				cIcon = cMicon[0];
			}
			break;
		case 'a':
			iy--;
			if (iy < 0) {
				iStatus = 0;
			}
			else if (iy == 0) {
				iStatus = 2;
				cIcon = cMicon[1];
			}
			else if (ix > 0 && ix <= X_MAX) {
				iStatus = 1;
				cIcon = cMicon[0];
			}
			break;
		case 'd':
			iy++;
			if (iy > Y_MAX + 1) {
				iStatus = 0;
			}
			else if (iy == Y_MAX + 1) {
				iStatus = 2;
				cIcon = cMicon[1];
			}
			else if (ix > 0 && ix <= X_MAX) {
				iStatus = 1;
				cIcon = cMicon[0];
			}
			break;
	}
	if (iStatus) Show();
	else cout << "遊戲結束" << endl;
}

int main()
{
	char cIn;
	int iStatus;
	CMouse mouseX;
	iStatus = mouseX.GetStatus();
	mouseX.Show();
	while (iStatus) {
		cIn = _getch();
		mouseX.Update(cIn);
		iStatus = mouseX.GetStatus();
	}
	system("pause");
    return 0;
}

