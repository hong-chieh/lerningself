// 07.ConstructorBasic.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"
using namespace std;

//無痛 11-28

class CMouse {
public:
	CMouse();
private:
	int ix, iy;
};

CMouse::CMouse() {
	ix = 1, iy = 1;
	cout << "ix = " << ix << "   " << "iy = " << iy << endl;
}

int main()
{
	cout << "物件前" << endl;
	CMouse mouseX;
	cout << "物件後" << endl;
	system("pause");
    return 0;
}

