// 07.ConstructorBasic.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"
using namespace std;

//無痛 11-28

class CMouse {
public:
	CMouse();
	~CMouse();
private:
	int ix, iy;
};

CMouse::CMouse() {
	ix = 1, iy = 1;
	cout << "建構元建立" << endl;
}

CMouse::~CMouse() {
	cout << "被解構ㄌ" << endl;
	system("pause");
}

void DesTest() {
	cout << "物件2前" << endl;
	CMouse mouseX2;
	cout << "物件2後" << endl;
}

int main()
{
	DesTest();
	cout << "物件前" << endl;
	CMouse mouseX;
	cout << "物件後" << endl;
	system("pause");
	DesTest();
	return 0;
}

