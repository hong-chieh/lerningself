// ObjectOrientatedMouse.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"
using namespace std;
#define X_MAX 10
#define Y_MAX 10

//無痛 12-23

class CMouse {
private:
	int ix, iy;
	int iStatus;
	char cIcon;
	char cMicon[2];
public:
	void init() {
		ix = 1;
		iy = 1;
		iStatus = 1;
		cMicon[0] = '@';
		cMicon[1] = 'Q'; //由陣列設定icon
		cIcon = cMicon[0];
	}
	void Show();
	void SetIcon(char a,char b);
	void SetPos(int x, int y);
	int Update(char cIn);
	int GetStatus() { return iStatus; }; 
};

void CMouse::SetIcon(char a, char b) { //外部設定icon跟位置的函式
	cIcon = cMicon[0] = a;
	cMicon[1] = b;
}

void CMouse::SetPos(int x, int y) {
	ix = x;
	iy = y;
}

int CMouse::Update(char cIn) {
	switch (cIn) {
	case 'w':
		ix--;
		if (ix < 0) {
			iStatus = 0;
		}
		else if (ix == 0) {
			iStatus = 2;
			cIcon = cMicon[1];
		}
		else if (iy >= 1 && iy <= Y_MAX) {
			iStatus = 1;
			cIcon = cMicon[0];
		}
		break;
	case 's':
		ix++;
		if (ix > X_MAX + 1) {
			iStatus = 0;
		}
		else if (ix == X_MAX + 1) {
			iStatus = 2;
			cIcon = cMicon[1];
		}
		else if (iy >= 1 && iy <= Y_MAX) {
			iStatus = 1;
			cIcon = cMicon[0];
		}
		break;
	case 'a':
		iy--;
		if (iy < 0) {
			iStatus = 0;
		}
		else if (iy == 0) {
			iStatus = 2;
			cIcon = cMicon[1];
		}
		else if (ix >= 1 && ix <= X_MAX) {
			iStatus = 1;
			cIcon = cMicon[0];
		}
		break;
	case 'd':
		iy++;
		if (iy > Y_MAX + 1) {
			iStatus = 0;
		}
		else if (iy == Y_MAX + 1) {
			iStatus = 2;
			cIcon = cMicon[1];
		}
		else if (ix >= 1 && ix <= X_MAX) {
			iStatus = 1;
			cIcon = cMicon[0];
		}
		break;
	}
	return iStatus;
}

void CMouse::Show() {
	system("cls");
	for (int i = 0;i < ix;i++) cout << endl;
	cout << setw(iy + 1) << setfill(' ') << cIcon << endl;
}

int main()
{
	char cIn;
	int iStatus;
	CMouse mouseX;
	mouseX.init();
	mouseX.SetPos(5, 5);
	mouseX.SetIcon('$', '*');
	mouseX.Show();
	iStatus = mouseX.GetStatus();
	while (iStatus) {
		cIn = _getch();
		iStatus = mouseX.Update(cIn);
		if (iStatus) mouseX.Show();
		else cout << "結束遊戲" << endl;
	}
	system("pause");
	return 0;
}

