// ObjectOrientatedMouse.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"
using namespace std;
#define X_MAX 10
#define Y_MAX 10

//無痛 12-15

class CMouse {
public:
	int ix, iy;
	int iStatus;
	char cIcon;
	void init() {
		ix = 1;
		iy = 1;
		iStatus = 1;
		cIcon = '@';
	}
	void Show();
	int Update(char cIn);
};

int CMouse::Update(char cIn) {
	switch (cIn) {
	case 'w':
		ix--;
		if (ix < 0) {
			iStatus = 0;
		}
		else if (ix == 0) {
			iStatus = 2;
			cIcon = 'Q';
		}
		else if (iy >= 1 && iy <= Y_MAX) {
			iStatus = 1;
			cIcon = '@';
		}
		break;
	case 's':
		ix++;
		if (ix > X_MAX + 1) {
			iStatus = 0;
		}
		else if (ix == X_MAX + 1) {
			iStatus = 2;
			cIcon = 'Q';
		}
		else if (iy >= 1 && iy <= Y_MAX) {
			iStatus = 1;
			cIcon = '@';
		}
		break;
	case 'a':
		iy--;
		if (iy < 0) {
			iStatus = 0;
		}
		else if (iy == 0) {
			iStatus = 2;
			cIcon = 'Q';
		}
		else if (ix >= 1 && ix <= X_MAX) {
			iStatus = 1;
			cIcon = '@';
		}
		break;
	case 'd':
		iy++;
		if (iy > Y_MAX + 1) {
			iStatus = 0;
		}
		else if (iy == Y_MAX + 1) {
			iStatus = 2;
			cIcon = 'Q';
		}
		else if (ix >= 1 && ix <= X_MAX) {
			iStatus = 1;
			cIcon = '@';
		}
		break;
	}
	return iStatus;
}

void CMouse::Show() {
	system("cls");
	for (int i = 0;i < ix;i++) cout << endl;
	cout << setw(iy + 1) << setfill(' ') << cIcon << endl;
}

int main()
{
	char cIn;
	int iStatus;
	CMouse mouseX1, mouseX2;
	mouseX1.init();
	mouseX1.Show();
	iStatus = mouseX1.iStatus;
	while (iStatus) {
		cIn = _getch();
		iStatus = mouseX1.Update(cIn);
		if (iStatus) mouseX1.Show();
		else {
			cout << "結束遊戲，換第二隻老鼠" << endl;
			system("pause");
		}
	}
	mouseX2.ix = 6;
	mouseX2.iy = 6;
	mouseX2.cIcon = '@';
	iStatus = mouseX2.iStatus = 1;
	mouseX2.Show();
	while (iStatus) {
		cIn = _getch();
		iStatus = mouseX2.Update(cIn);
		if (iStatus) mouseX2.Show();
		else cout << "結束遊戲" << endl;
	}
	system("pause");
	return 0;
}

