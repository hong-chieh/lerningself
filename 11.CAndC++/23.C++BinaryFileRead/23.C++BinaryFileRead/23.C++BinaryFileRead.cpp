// 23.C++BinaryFileRead.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"
using namespace std;

//無痛 11-41

int main()
{
	fstream objfile;
	double da;
	float fb;
	int iArray[3];
	objfile.open("Binary.bin", ios::binary | ios::in);
	if (!objfile.is_open()) {
		cout << "檔案無法開啟" << endl;
		system("pause");
		return 0;
	}
	objfile.read((char*)&da, sizeof(double));
	objfile.read((char*)&fb, sizeof(float));
	objfile.read((char*)&iArray, sizeof(int) * 3);
	objfile.close();
	cout << "da = " << da << endl;
	cout << "fb = " << fb << endl;
	for (int i = 0;i < 3;i++ )cout << "iArray[" << i << "] = "  << iArray[i] << endl;
	system("pause");
    return 0;
}

