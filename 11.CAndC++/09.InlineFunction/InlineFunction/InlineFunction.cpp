// InlineFunction.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"
using namespace std;

//無痛 11-22

inline int iMax(int a, int b) {
	return a > b ? a : b;
}

int main()
{
	int a = 5, b = 6;
	cout << "max(5,6) = " << iMax(a, b) << endl;
	system("pause");
    return 0;
}

