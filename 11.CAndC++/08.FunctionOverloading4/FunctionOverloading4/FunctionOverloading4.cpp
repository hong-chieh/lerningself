// FunctionOverloading4.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"
using namespace std;

//無痛 11-20

void printCH(char ch, int s = 1, int n = 5) {
	cout << setw(s) << setfill(' ');
	n = n - (s - 1) * 2;
	for (int i = 0;i < n;i++) cout << ch;
	cout << endl;
}

int main()
{
	cout << "1234567890" << endl;
	printCH('*');
	printCH('&', 2);
	for (int i = 3;i > 0;i--) printCH('*' + i, i,5);
	system("pause");
    return 0;
}

