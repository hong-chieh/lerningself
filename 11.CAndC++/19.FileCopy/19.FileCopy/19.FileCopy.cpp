// 19.FileCopy.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"
using namespace std;

int main()
{
	char ch;
	fstream infile, outfile;
	infile.open("first.txt", ios::in);
	outfile.open("copy.txt", ios::out);
	if (!infile.is_open() || !outfile.is_open()) {
		cout << "檔案無法開啟" << endl;
		system("pause");
		return 0;
	}
	while (!infile.eof()) {
		if ((ch = infile.get()) != EOF) {
			cout << ch;
			outfile.put(ch);              //outfile << ch.put(); 是錯的啊
		}
	}
	infile.close();
	outfile.close();
	system("pause");
    return 0;
}

