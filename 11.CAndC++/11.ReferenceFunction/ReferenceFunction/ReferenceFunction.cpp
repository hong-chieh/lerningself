// ReferenceFunction.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"
using namespace std;
//無痛 11-25

int &min(int &a, int &b) {
	return a > b ? b : a;
}

int main()
{
	int i = 3, j = 5;
	cout << "Before i = " << i << " j = " << j << " min = " << min(i, j) << endl;
	min(i, j) = 10;
	cout << "After min(i,j) = 10   i = " << i << " j = " << j << " min = " << min(i, j) << endl;
	i = min(i, j);
	cout << "After i = min(i,j)   i = " << i << " j = " << j << " min = " << min(i, j) << endl;
	system("pause");
    return 0;
}

