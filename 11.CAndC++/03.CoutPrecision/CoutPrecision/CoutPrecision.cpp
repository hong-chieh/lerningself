// CoutPrecision.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"
using namespace std;

//無痛 11-12

int main()
{
	float fVal[] = { 12.21, 123.321, 1234.4321 };

	cout << "原始值:" << setprecision(8);
	for (int i = 0;i < 3;i++) cout << setw(12) << fVal[i];
	cout << endl;

	cout << "三個數字:" << setprecision(3);
	for (int i = 0;i < 3;i++) cout << setw(12) << fVal[i];
	cout << endl;

	cout << "固定長度:" << setiosflags(ios::fixed);
	for (int i = 0;i < 3;i++) cout << setw(12) << fVal[i];
	cout << endl;

	cout << "科學記號:"; cout << scientific; //課本寫setiosflags(ios::scientific); 結果很奇怪
	for (int i = 0;i < 3;i++) cout << setw(12) << fVal[i];
	cout << endl;

	system("pause");
    return 0;
}

