// FunctionInClass.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"
using namespace std;

//無痛 11-30

class MonsterInfo {
public:
	char cName[20];
	int iHP;
	void InputInfo() {
		cout << "輸入名稱:" << endl;
		cin.getline(cName, 18);
		cout << "輸入HP:" << endl;
		cin >> iHP;
	}
	void PrintInfo() {
		cout << "名稱:" << cName << endl << "HP:" << iHP << endl;
	}
};

int main()
{
	MonsterInfo Mon;
	Mon.InputInfo();
	Mon.PrintInfo();
	system("pause");
	return 0;
}