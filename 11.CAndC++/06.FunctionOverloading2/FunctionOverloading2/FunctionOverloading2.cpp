// FunctionOverloading2.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"
using namespace std;

//無痛 11-18

void printX(void);
void printX(int);
void printX(char);
void printX(char, int);

int main()
{
	printX();
	printX(5);
	printX('^');
	printX('%', 7);
	system("pause");
    return 0;
}

void printX() {
	cout << "*" << endl;
}

void printX(int a) {
	for (int i = 0;i < a;i++) cout << "*";
	cout << endl;
}

void printX(char ch) {
	cout << ch << endl;
}

void printX(char ch, int a) {
	for (int i = 0;i < a;i++) cout << ch;
	cout << endl;
}