// NewAndDeleteFunction.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"
#define TEST "New & Delete"
using namespace std;

//無痛 11-27

char *getString(const char *pstring) {
	char *pcs;
	pcs = new char[strlen(pstring) + 1];
	strcpy_s(pcs, strlen(pstring) + 1, pstring);
	return pcs;
}

int main()
{
	char *pcs;
	pcs = getString(TEST);
	cout << "pcs = " << pcs << endl;
	delete [] pcs;
	system("pause");
    return 0;
}

