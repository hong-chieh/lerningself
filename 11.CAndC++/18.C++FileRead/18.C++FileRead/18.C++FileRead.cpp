// 18.C++FileRead.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"
using namespace std;

//無痛 11-34

int main()
{
	char cBuf[80];
	ifstream infile;
	infile.open("first.txt", ios::in);
	if (!infile.is_open()) {
		cout << "檔案無法開啟" << endl;
		system("pause");
		return 0;
	}
	infile >> cBuf;
	cout << cBuf << endl;
	system("pause");
    return 0;
}

