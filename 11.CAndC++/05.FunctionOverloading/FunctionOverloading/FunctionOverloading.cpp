// FunctionOverloading.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"
using namespace std;

//無痛 11-16

int add(int, int);
float add(float, float);
int main()
{
	int ia, ib;
	float fa, fb;
	cout << "輸入兩個整數(空格分開): ";
	cin >> ia; cin >> ib;
	cout << "輸入兩個浮點數(空格分開): ";
	cin >> fa; cin >> fb;
	cout << "int add = " << add(ia, ib) << endl;
	cout << "float add = " << add(fa, fb) << endl;
	system("pause");
    return 0;
}

int add(int a, int b) {
	return(a + b);
}

float add(float a, float b) {
	return(a + b);
}
