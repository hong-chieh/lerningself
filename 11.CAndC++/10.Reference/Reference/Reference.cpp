// Reference.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"
using namespace std;
void swap(int &, int &);

//無痛 11-23

int main()
{
	int ia = 5, ib = 10;
	cout << "ia = " << ia << " &ia = " << &ia << endl;
	cout << "ib = " << ib << " &ib = " << &ib << endl;
	swap(ia, ib);
	cout << "ia = " << ia << " &ia = " << &ia << endl;
	cout << "ib = " << ib << " &ib = " << &ib << endl;
	system("pause");
    return 0;
}

void swap(int &rx, int &ry) {
	int t;
	t = rx;
	rx = ry;
	ry = t;
	cout << "rx = " << rx << " &rx = " << &rx << endl;
	cout << "ry = " << ry << " &ry = " << &ry << endl;
}
