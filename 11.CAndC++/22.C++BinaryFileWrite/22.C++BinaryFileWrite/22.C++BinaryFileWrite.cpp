// 22.C++BinaryFileWrite.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"
using namespace std;

//無痛 11-40

int main()
{
	double da = 3.9999;
	float fb = 3.9999f;
	int iArray[3] = { 3,5,7 };
	fstream objfile;
	objfile.open("Binary.bin", ios::binary | ios::out);
	if (!objfile.is_open()) {
		cout << "檔案無法開啟" << endl;
		system("pause");
		return 0;
	}
	objfile.write((char*)&da, sizeof(double));
	objfile.write((char*)&fb, sizeof(float));
	objfile.write((char*)&iArray, sizeof(int) * 3);
	objfile.close();
	system("pause");
    return 0;
}

