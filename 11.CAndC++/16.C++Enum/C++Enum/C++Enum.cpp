// C++Enum.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"
using namespace std;

//無痛 11-31

enum Volcaloid {Miku, Rin, Len, Ruka, Kaito, Meiko} Singer;
inline Volcaloid operator++(Volcaloid &rs, int) {  //重要，需自行定義++，沒了跑不動
	return rs = (Volcaloid)(rs + 1);
}


int main()
{
	char cChiName[][10] = { "咪哭", "鈴", "連", "嚕卡", "凱豆", "咩口" };
	char cEngName[][10] = { "Miku", "Rin", "Len", "Ruka", "Kaito", "Meiko" };
	cout << "英文名字    中文名字" << endl;
	for (Singer = Miku;Singer < Meiko;Singer++) {
		cout << setiosflags(ios::left);
		cout << setw(10) << cChiName[Singer] << "   " << cEngName[Singer] << endl;
	}
	system("pause");
    return 0;
}

