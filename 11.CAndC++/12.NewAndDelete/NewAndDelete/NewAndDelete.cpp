// NewAndDelete.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"
#define TEST "New & Delete"
using namespace std;

//無痛 11-26

int main()
{
	int *pix = new int;
	*pix = sizeof(TEST);
	char *pcs = new char[*pix];
	memcpy(pcs, TEST, *pix);
	cout << "sizeof(\"" << pcs << "\") = " << *pix << endl;
	delete pix;
	delete [] pcs;
	system("pause");
    return 0;
}

