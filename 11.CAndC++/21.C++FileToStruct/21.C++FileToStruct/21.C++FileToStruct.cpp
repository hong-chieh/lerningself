// 21.C++FileToStruct.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"
using namespace std;

//無痛 11-38 隨機存取在11-39

struct MikuInfo {
	int id;
	char cName[15];
};

int main()
{
	ifstream infile;
	int Max = 0;
	struct MikuInfo Miku[5];
	infile.open("MikuInfo.txt", ios::in);
	if (!infile.is_open()) {
		cout << "檔案無法開啟" << endl;
	}
	while (!infile.eof()) {
		infile >> Miku[Max].id;
		infile >> Miku[Max].cName;
		Max++;
	}
	cout << setiosflags(ios::left);
	cout << setw(10) << "咪哭編號" << "咪哭名稱" << endl;
	for (int i = 0;i < Max;i++) {
		cout << setw(10) << Miku[i].id << Miku[i].cName << endl;
	}
	system("pause");
    return 0;
}

