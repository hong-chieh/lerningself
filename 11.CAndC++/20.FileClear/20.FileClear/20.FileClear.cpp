// 20.FileClear.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"
using namespace std;

//無痛 11-37

int main()
{
	char cBuf[80];
	fstream File;
	File.open("first.txt", ios::in); //開啟檔案 共兩行
	if (!File.is_open()) {           //無法開啟的處理
		cout << "檔案無法開啟" << endl;
		system("pause");
		return 0;
	}
	while (!File.eof()) {            //輸出至偵錯上
		File.getline(cBuf, 80);
		cout << cBuf << endl;
	}
	File.close();                     //關閉跟清空
	File.clear();
	File.open("copy.txt", ios::out);      //開啟輸出檔案
	if (!File.is_open()) {              //無法開啟的處理
		cout << "檔案無法開啟" << endl;
		system("pause");
		return 0;
	}
	File << cBuf;          //匯入檔案
	File.close();        //檔案關閉
	system("pause");
    return 0;
}

