// FunctionOverloading3.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"
using namespace std;

//無痛 11-19

int Damage(int Attack = 5, int Defense = 2);

int main()
{
	cout << "預設傷害為: " << Damage() << endl;
	cout << "攻擊為9的傷害為: " << Damage(9) << endl;
	//cout << "防禦為5的傷害為: " << Damge(, 5) << endl; //不行的
	cout << "攻擊為10，防禦3的傷害為: " << Damage(10, 3) << endl;
	system("pause");
    return 0;
}

int Damage(int A, int D) {
	return A * 2 - D;
}