// CoutAlignment.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"
using namespace std;

//無痛 11-11

int main()
{
	const char *cName[] = { "Miku","Rin","Len" };
	float fVal[] = { 100000.5, 2.111, 3.66666666 };
	for (int i = 0;i < 3;i++) {
		cout << setiosflags(ios::left);
		cout.width(9);
		cout << cName[i];
		cout << resetiosflags(ios::left);
		cout << setw(8) << fVal[i] << endl;
	}
	for (int i = 0;i < 3;i++) {
		cout.fill('@');
		cout << resetiosflags(ios::left);
		cout.width(10);
		cout << cName[i];
		cout.fill('/');
		cout << setiosflags(ios::left);
		cout << setw(8) << fVal[i] << endl;
	}
	system("pause");
    return 0;
}

