// ctypeTest.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"
void CharNumber(char ch);
int iLower, iUpper, iSpace, iPunct, iDigit, iAlpha;

//無痛 目前未找到

int main()
{
	char ch;
	printf("輸入一串英文字用 # 結尾:\n");
	iLower = iUpper = iSpace = iPunct = iDigit = iAlpha = 0;
	while((ch = getchar()) != '#') {
		CharNumber(ch);
	}
	printf("\n有 %d 個小寫\n", iLower);
	printf("有 %d 個大寫\n", iUpper);
	printf("有 %d 個數字\n", iDigit);
	printf("有 %d 個空白\n", iSpace);
	printf("有 %d 個標點符號\n", iPunct);
	printf("共 %d 個字母\n", iAlpha);
	system("pause");
    return 0;
}

void CharNumber(char ch) {
	if (islower(ch)) {
		printf("%c", toupper(ch));
		iAlpha++;
		iLower++;
	}
	else if (isupper(ch)) {
		printf("%c", tolower(ch));
		iAlpha++;
		iUpper++;
	}
	else {
		if (isdigit(ch)) iDigit++;
		if (isspace(ch)) iSpace++;
		if (ispunct(ch)) iPunct++;
		printf("%c", ch);
	}
}