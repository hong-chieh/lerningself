// Stack.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"
#define MAX 10
void pushFunction(int *);
void popFunction(int *);
void listFunction(int *);
char cStack[MAX][20];

int main()
{
	int iOpt, iTop;
	iTop = -1;
	
	while (true) {
		printf("請問想對堆疊陣列作什麼? 1.增加內容(push) 2.刪除內容(pop) 3.離開\n");
		scanf_s("%d", &iOpt);
		switch (iOpt)
		{
		default:
			break;
		case 1:
			iOpt = 0;
			pushFunction(&iTop);
			listFunction(&iTop);
			break;
		case 2:
			iOpt = 0;
			popFunction(&iTop);
			listFunction(&iTop);
			break;
		case 3:
			return 0;
		}
	}
}

void pushFunction(int *top) {
	if (*top >= MAX - 1) {
		printf("Stack已滿\n");
	}
	else {
		*top = *top + 1;
		printf("想增加什麼內容?\n");
		while (getchar() != '\n');
		gets_s(cStack[*top]);
	}
}

void popFunction(int *top) {
	if (*top <= -1) {
		printf("現在Stack是空的\n");
	}
	else {
		*top = *top - 1;
		printf("不理會最後的數字\n");
	}
}

void listFunction(int *top) {
	if (*top < 0) {
		printf("Stack是空的\n");
	}
	else {
		printf("\n\n");
		for (int i = 0;i <= *top;i++) {
			printf("%s\n", cStack[i]);
		}
		printf("\n");
	}
}

