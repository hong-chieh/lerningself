// Queue.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"
#define MAX 10

char cQueue[MAX][20];

int iFront = MAX - 1;
int iRear = MAX - 1;
bool isFull = false;
void enQueueFunction();
void deQueueFunction();
void listQueueFunction();

int main()
{
	int iOpt;
	while (true) {
		do {
			printf("請問你要怎麼處理佇列 1.增加內容 2.刪除內容 3.離開\n");
			scanf_s("%d", &iOpt);
		} while (iOpt < 1 || iOpt > 3);
		switch (iOpt)
		{
		default:
			break;
		case 1:
			enQueueFunction();
			listQueueFunction();
			break;
		case 2:
			deQueueFunction();
			listQueueFunction();
			break;
		case 3:
			return 0;
		}
	}
}

void enQueueFunction() {
	if (iRear == iFront && isFull) {
		printf("佇列已滿\n");
	}
	else {
		iRear = (iRear + 1) % MAX;
		printf("想加入什麼內容?\n");
		scanf_s("%s", cQueue[iRear],sizeof(cQueue[iRear]));
		if (iRear == iFront) {
			isFull = true;
		}
	}
}

void deQueueFunction() {
	if (iFront == iRear && !isFull) {
		printf("佇列是空的\n");
	}
	else {
		iFront = (iFront + 1) % MAX;
		printf("內容已刪除\n");
		if (iFront == iRear) isFull = false;
	}
}

void listQueueFunction() {
	if (iFront == iRear && !isFull) {
		printf("佇列是空的\n");
	}
	else {
		int i = (iFront + 1) % MAX, j;
		if (iRear >= i) {
			printf("\n佇列為:\n");
			for (j = i;j <= iRear;j++) {
				printf("%s\n", cQueue[j]);
			}
		}
		else {
			printf("\n佇列為:\n");
			for (j = i;j < MAX;j++) {
				printf("%s\n", cQueue[j]);
			}
			for (j = 0;j <= iRear;j++) {
				printf("%s\n", cQueue[j]);
			}
		}
	}
}

