// ConsoleApplication1.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"
#include "Header.h"
#include "HeaderA.h"


int main()
{
	MyClass C;
	for (int i = 0;i < 10;i++) {
		C.PrintIncrease();
	}

	MyClassAddAndSub* pCAS = new MyClassAddAndSub();
	for (int i = 0; i < 20; i++)
	{
		pCAS->PrintAddAndSub();
	}

	delete pCAS;
	pCAS = nullptr;
	
	system("pause");
    return 0;
}

