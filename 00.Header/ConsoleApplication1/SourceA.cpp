#include "HeaderA.h"
#include <stdio.h>

MyClassAddAndSub::MyClassAddAndSub()
{
}

MyClassAddAndSub::~MyClassAddAndSub()
{
}

void MyClassAddAndSub::PrintAddAndSub()
{
	oTest.nAdded++;
	oTest.nSubed--;
	printf("Add: %d, Sub: %d\n", oTest.nAdded, oTest.nSubed);
}