// MultiplicationTable.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"
#define ROW 9
#define COL 9

//無痛 6-30

int main()
{
	printf("九九乘法表:\n");
	for (int i = 0;i <= COL;i++) {
		if (i == 0) printf("    *");
		else printf("%5d", i);
	}
	for (int i = 1; i <= ROW;i++) {
		printf("\n");
		for (int j = 0;j <= COL;j++) {
			if (j == 0) printf("%5d", i);
			else printf("%5d", i * j);
		}
	}
	printf("\n");
	system("pause");
    return 0;
}

