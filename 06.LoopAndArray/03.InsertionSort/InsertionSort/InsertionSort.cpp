// InsertionSort.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"

//無痛 6-53

int main()
{
	int A[6] = { 23,31,3,19,54,12 };
	int t, Now;
	for (int i = 0; i < 6; i++) {
		printf("%d ", A[i]);
	}
	for (int i = 0; i < 6; i++) {
		printf("\n");
		Now = 0;
		t = A[i];
		while (A[Now] < t && Now < i) Now++;
		if (Now != i) {
			for (int j = i;j > Now;j--) A[j] = A[j - 1];
			A[Now] = t;
		}
		for (int j = 0;j <= i;j++) printf("%d ", A[j]);
	}
	system("pause");
    return 0;
}

