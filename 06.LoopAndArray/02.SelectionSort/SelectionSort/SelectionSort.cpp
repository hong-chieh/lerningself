// SelectionSort.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"

//無痛 6-50

int main()
{
	int A[6] = { 23,31,3,19,54,12 };
	int min, t;
	for (int i = 0;i < 6;i++) {
		printf("%d ",A[i]);
	}
	for (int i = 0;i < 5;i++) {
		printf("\n");
		min = i;
		for (int j = i + 1;j < 6;j++) {
			if (A[min] > A[j]) min = j;
		}
		t = A[i]; 
		A[i] = A[min];
		A[min] = t;
		for (int j = 0;j < 6;j++) printf("%d ", A[j]);
	}
	system("pause");
    return 0;
}

