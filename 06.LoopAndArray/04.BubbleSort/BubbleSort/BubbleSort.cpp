// BubbleSort.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"

//無痛 6-55

int main()
{
	int A[6] = { 23,31,3,19,54,12 };
	bool bSwitch;
	int t;
	for (int i = 0;i < 6;i++) printf("%d ",A[i]);
	printf("\n");
	do {
		bSwitch = false;
		for (int i = 0;i < 5;i++) {
			if (A[i] > A[i + 1]) {
				t = A[i + 1];
				A[i + 1] = A[i];
				A[i] = t;
				bSwitch = true;
				for (int j = 0;j < 6;j++) printf("%d ", A[j]);
				printf("\n");
			}
		}
	} while (bSwitch);


	system("pause");
    return 0;
}

