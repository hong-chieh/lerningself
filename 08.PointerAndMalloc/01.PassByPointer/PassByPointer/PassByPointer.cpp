// PassByPointer.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"
//int Sum(int, int);

//int Sum(int n, int total) {
//	for (int i = 1;i <= n;i++) total += i;
//	return total;
//}

//無痛 8-19

void Sum(int, int *);

void Sum(int n, int *total) {
	for (int i = 1;i <= n;i++) *total += i;
}

int main()
{
	int n, total = 0;
	printf("輸入n = ");
	scanf_s("%d", &n);
	Sum(n, &total);
	/*total = Sum(n, total);*/
	printf("1+2+...+%d = %d\n", n, total);
	system("pause");
    return 0;
}

