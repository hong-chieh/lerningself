// ConsoleApplication2.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"

//void swap(int, int);
//void swap(int a, int b) {
//	int t;
//	t = a;
//	a = b;
//	b = t;
//	printf("a: %d b:%d", a, b); //←只能在這裡用，要傳回去要指標，回傳值也沒辦法用兩個值
//}

//無痛 8-20

void swap(int *, int *); 
void swap(int *b, int *a) {
	int t;
	t = *a;
	*a = *b;
	*b = t;
}


int main()
{
	int a = 5, b = 10;
	printf("a: %d b:%d\n", a, b);
	swap(&a, &b);
	printf("a: %d b:%d\n", a, b);
	system("pause");
    return 0;
}

