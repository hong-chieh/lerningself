// PinterArrayAdd.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"
#define ROW 2
#define COL 3

//

int main()
{
	int A[ROW][COL] = { { 54,2,3 },{ 6,77,5 } };
	int B[ROW][COL] = { { 9,6,87 },{ 3,5,78 } };
	int C[ROW][COL] = { 0 };
	for (int i = 0;i < ROW;i++) {
		for (int j = 0;j < COL;j++) {
			*(C[i] + j) = *(A[i] + j) + *(B[i] + j);
			printf("%d ", *(C[i] + j));
		}
		printf("\n");
	}
	system("pause");
	return 0;
}

