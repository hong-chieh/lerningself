// PointerCharArraySelectionSort.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"
#define NUM 5

//無痛 8-36

//void Swap(char *, char*);
//void Swap(char *a, char *b) {
//	int pc;
//	for (int i = 0;i < 10;i++) {
//		pc = *(a+i);
//		*(a+i) = *(b+i);
//		*(b+i) = pc;
//	}
//}

int main()
{
	char cArray[NUM][10];
	char *pcA[NUM];
	char *pc;
	int min;
	printf("輸入想要的英文單字:\n");
	for (int i = 0;i < NUM;i++) {
		printf("輸入第 %d 個單字: ",i+1);
		pcA[i] = cArray[i];
		gets_s(cArray[i]);
	}
	for (int i = 0;i < NUM - 1;i++) {
		min = i;
		for (int j = i + 1;j < NUM;j++) {
			if (strcmp(pcA[min], pcA[j]) > 0) min = j;
		}
		if (min != i) {
			//Swap(pcA[min], pcA[i]);
			pc = pcA[min];
			pcA[min] = pcA[i];
			pcA[i] = pc;
		}
	}
	for (int i = 0;i < NUM;i++) {
		printf("排序後的第 %d 個單字為: %s\n", i + 1, pcA[i]);
	}
	system("pause");
    return 0;
}

