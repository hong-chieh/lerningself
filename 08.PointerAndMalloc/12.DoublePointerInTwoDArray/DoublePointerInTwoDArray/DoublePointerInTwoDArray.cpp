// DoublePointerInTwoDArray.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"

//無痛 8-42

int main()
{
	int dpArray[3][4] = {
		{1,2,3,4},{11,12,13,14},{21,22,23,24}
	};
	printf("**dpArray: %d\n",**dpArray);
	printf("dpArray+1: %p\n*dpArray+1:%p\n", dpArray + 1, *dpArray + 1);
	printf("*(dpArray+2)+1 : %p\n*(*(dpArray+2)+1): %d\n", *(dpArray + 2) + 1, *(*(dpArray + 2) + 1));
	system("pause");
    return 0;
}

