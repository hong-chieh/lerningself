// PointerFunction.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"

//無痛 8-25

int Add(int, int);
int Multi(int, int);
int Add(int a, int b) {
	int Sum;
	Sum = a + b;
	return(Sum);
}
int Multi(int a, int b) {
	int Mul;
	Mul = a * b;
	return(Mul);
}

int main()
{
	int m, n, t;
	int(*pfun)(int, int);
	printf("請輸入想要的兩個數字 中間使用空格\n");
	scanf_s("%d %d", &m, &n);
	pfun = Add;
	t = pfun(m, n);
	printf("%d %d相加為%d\n", m, n, t);
	pfun = Multi;
	t = pfun(m, n);
	printf("%d %d相乘為%d\n", m, n, t);
	system("pause");
    return 0;
}

