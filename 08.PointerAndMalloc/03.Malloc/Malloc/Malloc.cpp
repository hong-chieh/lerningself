// Malloc.cpp: 定義主控台應用程式的進入點。
//
//頁數8-23
#include "stdafx.h"

//無痛 8-23

int * StorageGet(int);
int * StorageGet(int n) {
	int *ps;
	if ((ps = (int *)malloc(sizeof(int)*n)) == NULL) {
		printf("記憶體不足");
		exit(0);
	}
	return (ps);
}


int main()
{
	int *pi;
	int n, sum;
	/*if ((pi = (int *)malloc(sizeof(int) * 5)) == NULL) {
		printf("記憶體不足");
		return 0;
	}*/
	pi = StorageGet(5);
	printf("想要幾個數字加總?\n");
	scanf_s("%d", &n);
	if (n >= 2) {
		sum = 0;
		if (n > 5) {
			free(pi);
			/*if ((pi = (int *)malloc(sizeof(int) * n)) == NULL) {
				printf("記憶體不足");
				return 0;
			}*/
			pi = StorageGet(n);
		}
		for (int i = 0;i < n;i++) {
			printf("第 %d 個數字是?  ", i+1);
			scanf_s("%d", pi + i);
			sum += *(pi + i);
		}
		printf("這些數字的總和為 %d", sum);
	}
	else {
		printf("輸入錯誤喔\n");
	}
	free(pi);
	system("pause");
    return 0;
}

