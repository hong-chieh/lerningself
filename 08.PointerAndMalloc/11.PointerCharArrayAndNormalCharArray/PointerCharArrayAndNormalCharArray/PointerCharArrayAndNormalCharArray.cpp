// PointerCharArrayAndNormalCharArray.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"

//無痛 8-40

int main()
{
	char String[7] = "Miku";
	//char *pMiku;
	const char *pString = "Miku"; //書上未加const 但現在似乎必加
	char StrArray[4][8] = { "I","love","Hatsune","Miku" };
	const char *pStrArray[4] = { "I","love","Hatsune","Miku" };//節省空間，用多少拿多少
	//pMiku = pString; //不能拿取const
	printf("Ssizeof:%d SAszieof:%d\n", sizeof(String), sizeof(StrArray[0]));//指標陣列只記記憶體開頭，是無法看字串大小的
	for (int i = 0;i < 4;i++)printf("%s ", *(pStrArray + i));
	system("pause");
    return 0;
}

