// PointArray2.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"
#define ROW 2
#define COL 3

//無痛 8-32

int main()
{
	int A[ROW][COL] = { { 54,2,3 },{ 6,77,5 } };
	int B[ROW][COL] = { { 9,6,87 },{ 3,5,78 } };
	int C[ROW][COL] = { 0 };
	int *piA, *piB, *piC;
	for (int i = 0;i < ROW;i++) {
		piA = A[i];
		piB = B[i];
		piC = C[i];
		for (int j = 0;j < COL;j++) {
			*(piC + j) = *(piA + j) + *(piB + j);
			printf("%d ", *(piC + j));
		}
		printf("\n");
	}
	system("pause");
    return 0;
}

