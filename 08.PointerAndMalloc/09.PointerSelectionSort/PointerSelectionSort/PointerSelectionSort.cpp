// PointerSelectionSort.cpp: 定義主控台應用程式的進入點。
//

//無痛 8-34

#include "stdafx.h"
void Swap(int *, int *);
void Swap(int *a, int *b) {
	int t;
	t = *a;
	*a = *b;
	*b = t;
}

int main()
{
	int A[6] = { 23,31,3,19,54,12 };
	int min, t, *piA;
	piA = A;
	for (int i = 0;i < 5;i++) {
		min = i;
		for (int j = i+1;j < 6;j++) {
			if (*(piA + min) > *(piA + j)) min = j;
		}
		Swap(piA + min, piA + i);
		/*t = *(piA + i);
		*(piA + i) = *(piA + min);
		*(piA + min) = t;*/
		for (int j = 0;j < 6;j++) printf("%d ", *(piA + j));
		printf("\n");
	}
	system("pause");
    return 0;
}

