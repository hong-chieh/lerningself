// PointerArrayAdd3.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"
#define ROW 2
#define COL 3

//無痛 8-33

int main()
{
	int A[ROW][COL] = { { 54,2,3 },{ 6,77,5 } };
	int B[ROW][COL] = { { 9,6,87 },{ 3,5,78 } };
	int C[ROW][COL] = { 0 };
	int *piA, *piB, *piC;
	piA = A[0];
	piB = B[0];
	piC = C[0];
	for (int i = 0;i < ROW*COL;i++) {
		*(piC + i) = *(piA + i) + *(piB + i);
		printf("%d ", *(piC + i));
		if ((i + 1) % COL == 0) printf("\n");
	}
	system("pause");
    return 0;
}

