// PointerArray1.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"

//無痛 8-27

int main()
{
	int x[3][4] = { 0 };
	for (int i = 0;i < 3;i++) {
		for (int j = 0;j < 4;j++) {
			printf("%d ", x[i][j]);
		}
		printf("\n");
	}

	*x[0] = 1;
	*(x[1] + 1) = 1;
	*(x[2] + 2) = 1;

	printf("\n");
	for (int i = 0;i < 3;i++) {
		for (int j = 0;j < 4;j++) {
			printf("%d ", x[i][j]);
		}
		printf("\n");
	}

	*(x[0] + 3) = 2;
	*(x[1] + 2) = 2;
	*(x[2] + 1) = 2;

	printf("\n");
	for (int i = 0;i < 3;i++) {
		for (int j = 0;j < 4;j++) {
			printf("%d ", x[i][j]);
		}
		printf("\n");
	}
	system("pause");
    return 0;
}

