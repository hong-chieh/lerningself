import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const tweetSchema = new Schema ({
  id { type: Number, required: true, unique: true},
  tweet: String
});

const Tweets = mongoose.model('Tweets', tweetSchema);

export default Tweets;
