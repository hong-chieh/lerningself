import mongoose from 'mongoose';
import env from './environment'

mongoose.Promise = global.Promise

const mongoUri = 'mongodb://${env.dbName}:${env.password}@${env.dbName}.documents.azure.com:${env.port}/?ssl=true&replicaSet=globaldb';

function connect() {
  return mongoose.connect(mongoUri, { useMongoClient: true});
}

module.export = {
  connect,
  mongoose
};
