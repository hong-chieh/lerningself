module.exports = {
  entry:'./src/js/components/client.js',
  output: {
    path:__dirname,
    filename:'bundle.js'
  },
  resolve: {
    extensions:['.js']
  },
  devServer:{
    // color:true,
    historyApiFallback:true,
    contentBase:'./',
    inline:true,
    // progress:true,
    port:3000
  },
  module: {
    loaders:[
      {
        loader:'babel-loader',
        query:{
          presets:['es2015','react', 'stage-1']
        },
        test:/\.js$/,
        exclude: /(node_modules|bower_components)/
      }
    ]
  }
};
