import { applyMiddleware, createStore} from 'redux';
import { createLogger} from 'redux-logger';
import thunk from 'redux-thunk';
import axios from 'axios';
import promise from 'redux-promise-middleware';

//註解為不用promise的ajax實現方式

const initialState = {
  Fetching: false,
  Fetched: false,
  Data: [],
  Err: null
}

const reducer = function(state = initialState, action){
  switch(action.type){
    case "FETCH_PENDING":
      return {...state, Fetching: true};
      break;
    case "FETCH_FULFILLED":
      return {...state, Fetching: false, Fetched:true, Data: action.payload};
      break;
    case "FETCH_REJECTED":
      return {...state, Fetching: false, Err: action.payload};
      break;
  }
  // switch (action.type) {
  //   case "FETCH_START":
  //     return {...state, Fetching: true};
  //     break;
  //   case "FETCH_SUCCESS":
  //     return {...state, Fetching: false, Fetched: true, Data: action.payload};
  //     break;
  //   case "FETCH_FAILED":
  //     return {...state, Fetching: false, Err: action.payload};
  // }
  return state;
}


const middleWare = applyMiddleware(thunk, promise(), createLogger());

const store = createStore(reducer, middleWare);

store.dispatch({
  type: "FETCH",
  payload: axios.get('http://rest.learncode.academy/api/wstern/users')
})

// store.dispatch((dispatch)=>{
//   dispatch({type: "FETCH_START"})
//   axios.get('http://rest.learncode.academy/api/wstern/users')
//     .then((response) =>{
//       dispatch({type: "FETCH_SUCCESS", payload: response.data})
//     })
//     .catch((err)=>{
//       dispatch({type: "FETCH_FAILED", payload: err})
//     })
// })
