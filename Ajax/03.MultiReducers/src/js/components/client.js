import { combineReducers, createStore} from "redux";

const userReducer = (state={}, action) => {
  switch (action.type) {
    case "ADD_NAME":{
      return {...state, name:action.payload};
      break;
      }
    case "ADD_AGE": {
      return {...state, age:action.payload};
      break;
    }
  }
  return state;
}

const tweetsReducer = (state=[], action) =>{
  switch(action.type){
    case "ADD_TWEET": {
      return state.concat({
        text: action.payload
      });
      break;
    }
  }
  return state;
}

const reducers = combineReducers({
  user: userReducer,
  tweets: tweetsReducer
})

const store = createStore(reducers);

store.subscribe(()=>{
  console.log("OK CHANGE ", store.getState())
});

store.dispatch({type:"ADD_NAME", payload:"PP"});
store.dispatch({type:"ADD_AGE", payload:35});
store.dispatch({type:"ADD_TWEET", payload:"Hello World!"});
store.dispatch({type:"ADD_TWEET", payload:"How are you"});
