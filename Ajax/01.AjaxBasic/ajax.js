(function(){
  var httpRequest;
  document.getElementById("ajaxButton").addEventListener('click',Request);
  function Request(){
    if(window.XMLHttpRequest){
      httpRequest = new XMLHttpRequest();
    }else if(window.ActiveXObject){
      httpRequest = ActiveXObject("Microsoft.XMLHTTP");
    }

    if(!httpRequest){
      alert("Cannot Find XMLHTTP");
      return false;
    }
    httpRequest.onreadystatechange = textResponse;
    httpRequest.open('GET','test.html');
    httpRequest.send(null);
  }
  function textResponse(){
    if(httpRequest.readyState == 4){
      if(httpRequest.status == 200){
        alert(httpRequest.responseText);
      }else if(httpRequest.status == 404){
        alert("404 error");
      }else if(httpRequest.status == 403){
        alert("403 error")
      }else {
        alert(httpRequest.status);
      }
    }
  }
})();
