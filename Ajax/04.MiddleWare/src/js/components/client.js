import { applyMiddleware, createStore} from 'redux';

const reducer = function(state, action){
  if(action.type == "ADD") return state + 1;
  else if(action.type == "DEC") return state -1;
  else if(action.type === "E") throw new Error("AHHH!");
  return state;
}

const logger = (store) => (next) => (action) => {
  console.log("Hello", action);
  next(action);
}

const error = (store) => (next) => (action) =>{
  try{
    next(action);
  }catch(e){
    console.log("Have an error", e);
  }
}

const middleWare = applyMiddleware(logger, error);

const store = createStore(reducer, 0, middleWare);

store.subscribe(() => {
  console.log("Changed", store.getState());
})

store.dispatch({type:"ADD"});
store.dispatch({type:"ADD"});
store.dispatch({type:"ADD"});
store.dispatch({type:"DEC"});
store.dispatch({type:"DEC"});
store.dispatch({type:"DEC"});
store.dispatch({type:"E"});
