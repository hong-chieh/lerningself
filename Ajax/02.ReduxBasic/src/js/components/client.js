import { createStore} from 'redux';

const reducer = function(state, action){
  if(action.type == "ADD") return state+action.number;
  if(action.type == "DEC")
    return state-action.number;
  return state;
}

const store = createStore(reducer,0);

store.subscribe(() => {
  console.log("Store Changed "+ store.getState());
});

store.dispatch({type:"ADD", number:1});
store.dispatch({type:"ADD", number:2});
store.dispatch({type:"ADD", number:5});
store.dispatch({type:"ADD", number:6});
store.dispatch({type:"ADD", number:88});
store.dispatch({type:"DEC", number:5})
