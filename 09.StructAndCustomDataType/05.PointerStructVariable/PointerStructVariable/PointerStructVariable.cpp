// PointerStructVariable.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"

//無痛 9-18

struct MonsterInfo
{
	char cName[10];
	int iHP;
	int iAttack;
	int iDefense;
} Puni = { "Puni",10,5,1 };

int main()
{
	struct MonsterInfo *pMon;
	pMon = &Puni;
	printf("%s 的血量 %d 攻擊力 %d 防禦力 %d\n", pMon->cName, pMon->iHP, pMon->iAttack, pMon->iDefense);
	system("pause");
    return 0;
}

