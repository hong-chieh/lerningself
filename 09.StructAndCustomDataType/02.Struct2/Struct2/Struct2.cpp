// Struct2.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"

//無痛 9-10

int main()
{
	struct GameInfo
	{
		char Name[10];
		int Price;
		char Info[100];
		float OnSale;
	} ZeldaBoTW = { "Zelda",1500,"Good Game",20 }, MarioOdessy = {"Mario",1600,"Very Good Game",30};
	
	printf("你的遊戲名稱為：%s\n你的遊戲價格為：%d\n折扣後的價格為：%0.0f\n遊戲簡介：%s\n", ZeldaBoTW.Name, ZeldaBoTW.Price, ZeldaBoTW.Price*(1 - ZeldaBoTW.OnSale / 100), ZeldaBoTW.Info);
	printf("你的遊戲名稱為：%s\n你的遊戲價格為：%d\n折扣後的價格為：%0.0f\n遊戲簡介：%s\n", MarioOdessy.Name, MarioOdessy.Price, MarioOdessy.Price*(1 - MarioOdessy.OnSale / 100), MarioOdessy.Info);
	system("pause");
	return 0;
}

