// ArrayStruct.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"
#define ANDROID 1
#define APPLE 2

//無痛 9-15

struct SmartPhone {
	char cName[20];
	char cVersion[10];
	int iSystem;
	float fSize;
};

int main()
{
	char System[2][5] = {"安卓","蘋果"};
	struct SmartPhone SP[3];
	for (int i = 0;i < 3;i++) {
		printf("您的第 %d 台是用什麼手機?\n", i + 1);
		if(i) while (getchar() != '\n');
		gets_s(SP[i].cName);
		printf("什麼版本?\n");
		gets_s(SP[i].cVersion);
		do {
			printf("什麼系統?(1.安卓 2.蘋果)\n");
			scanf_s("%d", &SP[i].iSystem);
		} while (SP[i].iSystem < 1 || SP[i].iSystem > 2);
		printf("什麼尺寸?\n");
		scanf_s("%f", &SP[i].fSize);
	}
	for (int i = 0;i < 3;i++) {
		int j;
		if (SP[i].iSystem == ANDROID) j = ANDROID -1;
		else if (SP[i].iSystem == APPLE) j = APPLE -1;
		printf("您第 %d 台手機為 %s\n%s 版的%s系統 尺寸為%2.1f\n\n", i + 1, SP[i].cName, SP[i].cVersion, System[j], SP[i].fSize);
	}
	system("pause");
    return 0;
}

