// StructArray.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"

//無痛 9-12

int main()
{
	struct Miku {
		char MikuName[10];
		int iScore[3];
		float fAve;
	};
	Miku SinKaiMiku;
	const char *SinKai1 = "深海少女" ;
	const char *SinKai2 = "深海咪哭";
	printf("請問深海少女的名字是:\n");
	gets_s(SinKaiMiku.MikuName);
	if (strcmp(SinKaiMiku.MikuName,SinKai1) == 0 || strcmp(SinKaiMiku.MikuName, SinKai2) == 0) {
		printf("這位咪哭的可愛程度幾分?\n");
		scanf_s("%d", &SinKaiMiku.iScore[0]);
		printf("這位咪哭的歌唱程度幾分?\n");
		scanf_s("%d", &SinKaiMiku.iScore[1]);
		printf("這位咪哭的跳舞程度幾分?\n");
		scanf_s("%d", &SinKaiMiku.iScore[2]);
		for (int i = 0;i < 3;i++) {
			if (SinKaiMiku.iScore[i] < 100) return 0;
			SinKaiMiku.fAve += SinKaiMiku.iScore[i];
		}
		SinKaiMiku.fAve /= 3;
		printf("%s 的平均分數是: %1.0f\n", SinKaiMiku.MikuName, SinKaiMiku.fAve);
		system("pause");
		return 0;
	}
	else {
		printf("%s", SinKaiMiku.MikuName);
		system("pause");
		return 0;
	}
}

