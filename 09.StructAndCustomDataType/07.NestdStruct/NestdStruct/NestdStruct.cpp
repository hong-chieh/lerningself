// NestdStruct.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"

//無痛 9-23

struct MonInfo
{
	char cName[10];
	int iHP;
	int iAttack;
	int iDefense;
};

struct BossInfo {
	struct MonInfo info;
	int iHP2;
	int iAttackArea;
};

int main()
{
	struct BossInfo Miku = { "Miku",20,8,100,20,5 };
	printf("名稱: %s 血量: %d 第二段血量: %d 攻擊力: %d 防禦力: %d 攻擊範圍: %d\n", Miku.info.cName, 
		Miku.info.iHP, Miku.iHP2, Miku.info.iAttack, Miku.info.iDefense, Miku.iAttackArea);
	system("pause");
    return 0;
}

