// Union.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"

//無痛 9-30

union Paid{
	char cCreditCard[17];
	int iCash;
} money; //<-重要!!!!!!!!

int main()
{
	int iAmt = 1500;
	int iPay = 0;
	printf("應付金額為： %d\n", iAmt);
	do {
		printf("您要用信用卡或者現金付款: 1.信用卡 2.現金：\n");
		scanf_s("%d", &iPay);
		if (iPay == 1) {
			printf("請輸入卡號:\n");
			while (getchar() != '\n');
			gets_s(money.cCreditCard);
			if (strlen(money.cCreditCard) != 16) {
				printf("輸入錯誤\n");
				iPay = 0;
			}
			else printf("付款成功!\n");
		}
		else if (iPay == 2) {
			printf("請輸入付款金額:\n");
			scanf_s("%d", &money.iCash);
			if (money.iCash < iAmt) {
				printf("金額不足\n");
				iPay = 0;
			}
			else {
				printf("付款成功!\n");
				if (money.iCash > iAmt) printf("找您 %d 元\n", money.iCash - iAmt);
			}
		}
		else printf("輸入錯誤\n");
	} while (iPay != 1 && iPay != 2);
	system("pause");
    return 0;
}

