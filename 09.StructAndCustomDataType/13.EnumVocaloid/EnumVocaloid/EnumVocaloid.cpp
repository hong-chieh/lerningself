// EnumVocaloid.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"

//無痛 9-34

enum Vocaloid {
	MIKU, RIN, LEN, RUKA, KAITO, MEIKO
};

int main()
{
	char cEnName[][15] = {
		"HatsuneMiku","KagamineRin","KagamineLen","MegurineRuka","Kaito","Meiko"
	};
	char cChName[][15] = {
		"初音未來","鏡音鈴","鏡音連","巡音流歌","Kaito","Meiko"
	};
	printf("日文名稱       英文名稱\n");
	for (int i = MIKU;i <= MEIKO;i++) {
		printf("%-15s%s\n", cChName[i], cEnName[i]);
	}
	system("pause");
    return 0;
}

