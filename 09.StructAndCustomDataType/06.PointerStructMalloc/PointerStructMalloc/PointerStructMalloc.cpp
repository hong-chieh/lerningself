// PointerStructMalloc.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"

//無痛 9-21

struct MonInfo
{
	char cName[10];
	int iHP;
	int iAttack;
	int iDefense;
};

int main()
{
	struct MonInfo *pMon;
	if ((pMon = (struct MonInfo *)malloc(sizeof(struct MonInfo))) == NULL) {
		printf("記憶體不足");
		system("pause");
		return 0;
	}
	printf("怪物名稱\n");
	gets_s(pMon->cName);

	printf("血量\n");
	scanf_s("%d", &pMon->iHP);

	printf("攻擊力\n");
	scanf_s("%d", &pMon->iAttack);

	printf("防禦力\n");
	scanf_s("%d", &pMon->iDefense);
	printf("%s 血量: %d 攻擊力: %d 防禦力: %d\n", pMon->cName, pMon->iHP, pMon->iAttack, pMon->iDefense);
	system("pause");
    return 0;
}

