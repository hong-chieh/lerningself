// Struct1.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"

//無痛 9-7

int main()
{
	struct GameInfo
	{
		char Name[10];
		int Price;
		char Info[100];
		float OnSale;
	};
	struct GameInfo ZeldaBoTW;
	printf("輸入你的遊戲名稱\n");
	gets_s(ZeldaBoTW.Name);
	printf("輸入你的遊戲簡介(100字內)\n");
	gets_s(ZeldaBoTW.Info);
	printf("輸入你的遊戲價格\n");
	scanf_s("%d", &ZeldaBoTW.Price);
	printf("輸入折扣為多少%%off(輸入整數即可)\n");
	scanf_s("%f", &ZeldaBoTW.OnSale);
	printf("你的遊戲名稱為：%s\n你的遊戲價格為：%d\n折扣後的價格為：%f\n遊戲簡介：%s\n", ZeldaBoTW.Name, ZeldaBoTW.Price, ZeldaBoTW.Price*(1 - ZeldaBoTW.OnSale / 100), ZeldaBoTW.Info);
	system("pause");
	return 0;
}
