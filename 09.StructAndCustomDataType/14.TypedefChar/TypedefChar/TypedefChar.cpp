// TypedefChar.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"
typedef char STRING[10];
typedef const char *PCHAR;

//無痛 9-36

int main()
{
	STRING sName1 = "Miku";
	PCHAR sName2 = "Luka";
	printf("%s 的容量大小 %d\n", sName1, sizeof(sName1));
	printf("%s 的容量大小 %d\n", sName2, sizeof(sName2));
	system("pause");
    return 0;
}

