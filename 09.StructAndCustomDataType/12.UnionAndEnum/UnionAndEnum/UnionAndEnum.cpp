// UnionAndEnum.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"

//無痛 9-31

struct Acura {
	int iFly;
	int iWeaponPower;
};

struct Gunvolt {
	int iThunderPower;
	int iSpecialPower;
};

enum identity {
	ACURA = 1,
	GUNVOLT
};

struct Role {
	int iHP;
	enum identity iType;
	union
	{
		struct Acura AR;
		struct Gunvolt GV;
	};
} OR;

int main()
{
	OR.iHP = 100;
	do {
		printf("請問你想選哪位角色? 1.亞裘拉 2.剛佛特\n");
		scanf_s("%d", &OR.iType);
	} while (OR.iType != ACURA && OR.iType != GUNVOLT);
	if (OR.iType == ACURA) {
		do {
			printf("需要幾個飛行插槽(最多5個):\n");
			scanf_s("%d", &OR.AR.iFly);
		} while (OR.AR.iFly < 1 || OR.AR.iFly > 5);
		OR.AR.iWeaponPower = 200;
		printf("你選擇了亞裘拉\nHP為 %d\n飛行插槽有 %d 個\n武器能量為 %d\n", OR.iHP, OR.AR.iFly, OR.AR.iWeaponPower);
	}
	else if (OR.iType == GUNVOLT) {
		do {
			printf("閃電能量多寡(最高1000 最低100)\n");
			scanf_s("%d", &OR.GV.iThunderPower);
		} while (OR.GV.iThunderPower < 100 || OR.GV.iThunderPower > 1000);
		OR.GV.iSpecialPower = 3;
		printf("你選擇了剛佛特\nHP為 %d\n特殊技能源有 %d 個\n閃電能量為 %d\n", OR.iHP, OR.GV.iSpecialPower, OR.GV.iThunderPower);
	}
	system("pause");
    return 0;
}

