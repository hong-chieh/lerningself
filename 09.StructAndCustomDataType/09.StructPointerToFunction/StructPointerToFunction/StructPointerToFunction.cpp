// StructPointerToFunction.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"

//無痛 9-25

struct MonInfo {
	char cName[10];
	int iHP;
};

void ModifyMonInfo(struct MonInfo *);

int main()
{
	struct MonInfo Puni = { "Puni", 50 };
	printf("%s 血量:%d\n", Puni.cName, Puni.iHP);
	ModifyMonInfo(&Puni);
	printf("%s 血量:%d\n", Puni.cName, Puni.iHP);
	system("pause");
	return 0;
}

void ModifyMonInfo(struct MonInfo *Mon) {
	strcpy_s(Mon->cName, "Slim");
	Mon->iHP = 20;
	printf("%s 血量:%d\n", Mon->cName, Mon->iHP);
}