// LinkedList.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"

//無痛 9-39

typedef struct TNode {
	int Num;
	struct TNode *Link;
} NODE, *PNODE;

int main()
{
	int n, i;
	PNODE pHead, pTail, pNow;
	pHead = pTail = pNow = NULL;
	do {
		printf("需要幾個數字的串列?");
		scanf_s("%d", &n);
	} while (n < 1);
	if ((pHead = (TNode *)malloc(sizeof(TNode))) == NULL) {
		printf("記憶體不足\n");
		exit(0);
	}
	pTail = pHead;
	pHead->Link = NULL;
	printf("第 1 個數字是?\n");
	scanf_s("%d", &pHead->Num);
	for (i = 2;i <= n;i++) {
		if ((pNow = (TNode *)malloc(sizeof(TNode))) == NULL) {
			printf("記憶體不足\n");
			exit(0);
		}
		pNow->Link = NULL; //先清空!!很重要!!!
		pTail->Link = pNow;
		pTail = pNow;
		printf("第 %d 個數字是?\n", i);
		scanf_s("%d", &pTail->Num);
	}
	pNow = pHead;
	i = 1;
	while (pNow != NULL) {
		printf("第 %d 個數字是:%d\n", i, pNow->Num);
		pNow = pNow->Link;
		i++;
	}
	free(pHead);
	free(pNow);
	system("pause");
    return 0;
}

