// TypedefStructPointerToFunction.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"

//無痛 9-37

typedef struct MikuInfo {
	int iClothes;
	int iSong;
} Miku, *pMiku;
void printInfo(pMiku);
int main()
{
	Miku SinkaiMiku = { 100,101 };
	printInfo(&SinkaiMiku);
	printf("SinkaiMiku\nClothes:%-5d iSong:%d\n", SinkaiMiku.iClothes, SinkaiMiku.iSong);
	system("pause");
    return 0;
}

void printInfo(pMiku Miku) {
	printf("SinkaiMiku\nClothes:%-5d iSong:%d\n", Miku->iClothes, Miku->iSong);
	Miku->iClothes++;
	Miku->iSong++;
}

