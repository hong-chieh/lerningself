// StrcutArrayToFunction.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"

//無痛 9-26

struct MonInfo {
	char cName[10];
	int iHP;
};

void ModifyMonInfo(struct MonInfo []);

int main()
{
	struct MonInfo Mon[2] = { "Puni", 50, "Slim", 20 };

	ModifyMonInfo(Mon);
	for (int i = 0;i < 2;i++) printf("%s 血量:%d\n", Mon[i].cName, Mon[i].iHP); //不需要指標就能直接替換囉
	system("pause");
	return 0;
}

void ModifyMonInfo(struct MonInfo Mon[]) {
	for (int i = 0;i < 2;i++) {
		printf("%s 血量:%d\n", Mon[i].cName, Mon[i].iHP);
		Mon[i].iHP++;
	}
}