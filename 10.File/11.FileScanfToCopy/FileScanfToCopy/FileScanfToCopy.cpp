// FileScanfToCopy.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"

//無痛 10-19

int main()
{
	FILE *pIn, *pOut;
	char ch[20];
	char c;
	fopen_s(&pIn, "MikuIntro.txt", "r");
	fopen_s(&pOut, "MikuIntroOut.txt", "w");
	if (pIn == NULL || pOut == NULL) {
		printf("檔案開啟失敗");
		system("pause");
		return 0;
	}
	while (!feof(pIn)) {
		if (fscanf_s(pIn, "%s", ch, sizeof(ch)) != EOF) {
			fprintf(pOut, "%s", ch);
		}
		c = fgetc(pIn);
		c = fputc(c, pOut);
	}
	if(fclose(pIn) == EOF || fclose(pOut) == EOF) printf("檔案關閉失敗");
	else printf("檔案正常關閉");
	system("pause");
    return 0;
}

