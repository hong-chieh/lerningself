// UnbufferBinary.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"

//無痛 10-31

int main()
{
	int ifd;
	double da;
	float fb;
	int iArray[3];
	_sopen_s(&ifd, "Binary.bin", O_RDONLY, SH_DENYWR, S_IREAD);
	_read(ifd, &da, sizeof(double));
	_read(ifd, &fb, sizeof(float));
	_read(ifd, iArray, sizeof(int) * 3);
	printf("da = %lf\n", da);
	printf("fb = %f\n", fb);
	for (int i = 0;i < 3;i++)printf("iArray[%d] = %d\n", i, iArray[i]);
	_close(ifd);
	system("pause");
    return 0;
}

