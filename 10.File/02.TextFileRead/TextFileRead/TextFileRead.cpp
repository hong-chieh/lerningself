// TextFileRead.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"

//無痛 10-7

int main()
{
	char ch;
	FILE *pFile;
	fopen_s(&pFile, "Miku.txt", "r");
	if (pFile == NULL) {
		printf("無法讀取");
		system("pause");
		return 0;
	}
	while (feof(pFile) == 0) {
		ch = fgetc(pFile);
		putchar(ch);
	}
	fclose(pFile);
	system("pause");
    return 0;
}

