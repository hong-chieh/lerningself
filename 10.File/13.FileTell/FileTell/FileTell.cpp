// FileTell.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"

//無痛 10-22

int main()
{
	FILE *pFile;
	char cBuf[30];
	fopen_s(&pFile, "MikuIntro.txt", "r");
	if (pFile == NULL) {
		printf("檔案開啟失敗\n");
		system("pause");
		return 0;
	}
	while (!feof(pFile)) {
		if(fscanf_s(pFile, "%s", cBuf, sizeof(cBuf)) != EOF)
			printf("讀取字串: %-15s現在位置: %d\n", cBuf, ftell(pFile));
	}
	if (fclose(pFile) == EOF) printf("檔案關閉失敗\n");
	else printf("檔案正常關閉\n");
	system("pause");
	return 0;
}

