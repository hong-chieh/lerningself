// FileGetAndPutToCopy.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"

//無痛 10-18

int main()
{
	FILE *pIn, *pOut;
	char ch;
	fopen_s(&pIn, "MikuIntro.txt", "r");
	fopen_s(&pOut, "MikuIntroOut.txt", "w");
	if (pIn == NULL || pOut == NULL) {
		printf("檔案開啟失敗\n");
		system("pause");
		return 0;
	}
	while (!feof(pIn)) {
		ch = fgetc(pIn);
		fputc(ch, pOut);
	}
	if(fclose(pIn) == EOF || fclose(pOut) == EOF) printf("檔案關閉失敗\n");
	else printf("檔案正常關閉\n");
	system("pause");
    return 0;
}

