// UnbufferCopy.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"

//無痛 10-30

int main()
{
	int ifdIn, ifdOut, iLen;
	char cBuf[20];
	_sopen_s(&ifdIn, "MikuIntro.txt", O_RDONLY, SH_DENYWR, S_IREAD);
	_sopen_s(&ifdOut, "MikuIntroOut.txt", O_WRONLY | O_CREAT, SH_DENYNO, S_IWRITE);
	if (ifdIn == -1 || ifdOut == -1) {
		printf("檔案開啟失敗\n");
		system("pause");
		return 0;
	}
	while (!_eof(ifdIn)) {
		iLen = _read(ifdIn, cBuf, 16);
		_write(ifdOut, cBuf, iLen);
		cBuf[iLen] = '\0';
		printf("%s\n", cBuf);
	}
	_close(ifdIn);
	_close(ifdOut);
	system("pause");
    return 0;
}

