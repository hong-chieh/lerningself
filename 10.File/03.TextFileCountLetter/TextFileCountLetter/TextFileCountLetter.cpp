// TextFileCountLetter.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"

//無痛 10-9

int main()
{
	int i = 0;
	char ch;
	FILE *pFile;
	fopen_s(&pFile, "Miku.txt", "r");
	if (pFile == NULL) {
		printf("無法讀取");
		system("pause");
		return 0;
	}
	while (feof(pFile) == 0) {
		ch = fgetc(pFile);
		putchar(ch);
		if (isalpha(ch)) i++;
	}
	printf("有 %d 個英文字\n", i);
	if (fclose(pFile) == EOF) printf("檔案無法關閉\n");
	else printf("檔案關閉成功\n");
	system("pause");
	return 0;
}
