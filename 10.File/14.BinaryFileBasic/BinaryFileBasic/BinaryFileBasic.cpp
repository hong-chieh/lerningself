// BinaryFileBasic.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"

//無痛 10-24

int main()
{
	FILE *pFile;
	double da = 3.14159;
	float fb = 3.14159;
	int iArray[] = { 3,5,9 };
	fopen_s(&pFile, "Binary.bin", "wb");
	if (pFile == NULL) {
		printf("檔案開啟失敗\n");
		system("pause");
		return 0;
	}
	fwrite(&da, sizeof(double), 1, pFile);
	fwrite(&fb, sizeof(float), 1, pFile);
	fwrite(iArray, sizeof(int), 3, pFile);
	if (fclose(pFile) == EOF) printf("檔案關閉失敗\n");
	else printf("檔案正常關閉\n");
	system("pause");
	return 0;
}

