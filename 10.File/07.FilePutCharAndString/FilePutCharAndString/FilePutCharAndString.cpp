// FilePutCharAndString.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"

//無痛 10-15

int main()
{
	FILE *pFile;
	char cMiku[20] = "Miku is so cute.";
	int iLength;
	fopen_s(&pFile, "out.txt", "w");
	if (pFile == NULL) {
		printf("檔案開啟失敗\n");
		system("pause");
		return 0;
	}
	fputs(cMiku, pFile);
	fputs("\n", pFile); //相當於fputc的單引號\n
	iLength = strlen(cMiku);
	for (int i = 0;i < iLength;i++) {
		fputc(cMiku[i], pFile);
		fputc('\n', pFile); //相當於fputs的雙引號\n
	}
	if (fclose(pFile) == EOF) printf("檔案無法正常關閉\n");
	else printf("檔案正常關閉\n");
	system("pause");
    return 0;
}

