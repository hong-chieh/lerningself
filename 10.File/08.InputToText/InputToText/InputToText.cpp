// InputToText.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"

//無痛 10-16

struct GameInfo {
	int iID;
	char cName[10];
	int iPrice;
} *pNewGame;

int main()
{
	FILE *pFile;
	int n;
	fopen_s(&pFile, "out.txt", "w");
	printf("需要輸入幾款遊戲的資料?\n");
	scanf_s("%d", &n);
	if ((pNewGame = (GameInfo *)malloc(sizeof(GameInfo)*n)) == NULL) {
		printf("記憶體不足\n");
		system("pause");
		return 0;
	}
	for (int i = 0;i < n;i++) {
		printf("遊戲編號多少?\n");
		scanf_s("%d", &(pNewGame+i)->iID);
		printf("遊戲名稱是?\n");
		scanf_s("%s", (pNewGame+i)->cName, sizeof((pNewGame+i)->cName));
		printf("遊戲價格是?\n");
		scanf_s("%d", &(pNewGame+i)->iPrice);
	}
	for (int i = 0;i < n;i++) {
		fprintf(pFile, "%-6d %-9s %4d\n", (pNewGame+i)->iID, (pNewGame+i)->cName, (pNewGame+i)->iPrice);
	}
	if (fclose(pFile) == EOF) printf("檔案無法正常關閉\n");
	else printf("檔案正常關閉\n");
	free(pNewGame);
	system("pause");
    return 0;
}

