// StringReadToStruct.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"

//無痛 10-12

struct MikuInfo {
	int iId;
	char cName[10];
} Miku[4];

int main()
{
	FILE *pFile;
	int i = 0;
	fopen_s(&pFile, "MikuInfo.txt", "r");
	if (pFile == NULL) {
		printf("檔案開啟失敗\n");
		system("pause");
		return 0;
	}
	while (!feof(pFile)) {
		fscanf_s(pFile, "%d %s", &Miku[i].iId, Miku[i].cName, sizeof(Miku[i].cName));
		i++;
	}
	for (i = 0;i < 4;i++) {
		printf("%-5d %s\n", Miku[i].iId, Miku[i].cName);
	}
	if (fclose(pFile) == EOF) printf("檔案無法正常關閉\n");
	else printf("檔案關閉成功\n");
	system("pause");
    return 0;
}

