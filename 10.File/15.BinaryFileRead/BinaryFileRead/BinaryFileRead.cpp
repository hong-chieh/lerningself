// BinaryFileRead.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"

//無痛 10-25

int main()
{
	FILE *pFile;
	double da;
	float fb;
	int iArray[3];
	fopen_s(&pFile, "Binary.bin", "rb");
	if (pFile == NULL) {
		printf("檔案開啟失敗\n");
		system("pause");
		return 0;
	}
	fread(&da, sizeof(double), 1, pFile);
	fread(&fb, sizeof(float), 1, pFile);
	fread(iArray, sizeof(int), 3, pFile);
	printf("da: %lf\n", da);
	printf("fb: %f\n", fb);
	for (int i = 0;i < 3;i++) printf("iArray: %d\n", iArray[i]);
	if (fclose(pFile) == EOF) printf("檔案關閉失敗\n");
	else printf("檔案正常關閉\n");
	system("pause");
	return 0;
}

