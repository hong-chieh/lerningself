// UnbufferBasic.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"

//無痛 10-28

int main()
{
	char cStr[20] = "無緩衝檔案存取";
	char cBuf[50];
	int iLen;
	int ifd;
	_sopen_s(&ifd, "Unbuffer.txt", O_CREAT | O_RDWR, SH_DENYNO, S_IWRITE);
	if (ifd == -1) {
		printf("檔案無法開啟");
		system("pause");
		return 0;
	}
	_write(ifd, cStr, sizeof(cStr));

	_lseek(ifd, 0, SEEK_SET);
	iLen = _read(ifd, cBuf, sizeof(cBuf));
	cBuf[iLen] = '\0';
	printf("%s", cBuf);

	_close(ifd);
	system("pause");
    return 0;
}

