// RandomFileAccess.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"

//無痛 10-21

int main()
{
	FILE *pFile;
	char cBuf[30];
	fopen_s(&pFile, "MikuIntro.txt", "r");
	if (pFile == NULL) {
		printf("檔案開啟失敗\n");
		system("pause");
		return 0;
	}
	fseek(pFile, 3, SEEK_SET);
	fscanf_s(pFile, "%s", cBuf, sizeof(cBuf));
	printf("讀入資料為: %s\n", cBuf);
	
	rewind(pFile);
	fscanf_s(pFile, "%s", cBuf, sizeof(cBuf));
	printf("讀入資料為: %s\n", cBuf);
	
	fseek(pFile, -16, SEEK_END);
	fscanf_s(pFile, "%s", cBuf, sizeof(cBuf));
	printf("讀入資料為: %s\n", cBuf);
	if(fclose(pFile) == EOF) printf("檔案關閉失敗\n");
	else printf("檔案正常關閉\n");
	system("pause");
    return 0;
}

