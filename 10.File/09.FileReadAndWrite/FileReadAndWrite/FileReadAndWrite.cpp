// FileReadAndWrite.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"

//無痛 10-17

int main()
{
	FILE *pFIn, *pFOut;
	char cBuf[15];
	int iLen;
	fopen_s(&pFIn, "MikuIntro.txt", "r");
	fopen_s(&pFOut, "MikuIntroOut.txt", "w");
	if (pFIn == NULL || pFOut == NULL) {
		printf("檔案無法開啟\n");
		system("pause");
		return 0;
	}
	while (!feof(pFIn)) {
		iLen = fread(cBuf, sizeof(char), 12, pFIn);
		fwrite(cBuf, sizeof(char), iLen, pFOut); //iLen不一定是12
		cBuf[iLen] = '\0'; //不加的話會顯示亂碼喔
		printf("%d 個讀入:%s\n", iLen, cBuf);
	}
	if (fclose(pFIn) == EOF || fclose(pFOut) == EOF) printf("檔案無法正常關閉\n");
	else printf("檔案正常關閉\n");
	system("pause");
    return 0;
}

