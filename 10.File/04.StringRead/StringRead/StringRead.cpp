// StringRead.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"

//無痛 10-10

int main()
{
	FILE *pFile;
	char cSearch[10], cFile[60];
	bool isFound = false;
	int iLength;
	fopen_s(&pFile, "MikuInfo.txt", "r");
	if (pFile == NULL) {
		printf("無法讀取檔案");
		system("pause");
		return 0;
	}
	printf("請輸入搜尋編號:\n");
	scanf_s("%s", cSearch, sizeof(cSearch));
	iLength = strlen(cSearch); //如果用sizeof會連空白都算進去喔！要注意
	while (feof(pFile) == 0) {
		fgets(cFile, 60, pFile);
		if (strncmp(cFile, cSearch, iLength) == 0) {
			printf("找到資料\n%s", cFile);
			isFound = true;
		}
	}
	if (!isFound) {
		printf("找不到資料\n");
	}
	if (fclose(pFile) == EOF) {
		printf("無法正常關閉檔案\n");
	}
	else printf("檔案正常關閉\n");
	system("pause");
    return 0;
}

