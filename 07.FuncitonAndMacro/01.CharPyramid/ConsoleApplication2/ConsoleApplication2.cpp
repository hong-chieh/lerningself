// ConsoleApplication2.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"
#define HEIGHT 7
void ShowChar(char, int);

//無痛7-16

int main()
{
	for (int i = 0;i < HEIGHT;i++) {
		ShowChar(32,i);
		ShowChar(65 + i, 2*(HEIGHT - i) - 1);
		printf("\n");
	}
	system("pause");
	return 0;
}

void ShowChar(char En, int Num) {
	for (int i = 0;i < Num;i++) {
		printf("%c", En);
	}
}
