// PrimeNumberOneToThousand.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"
bool IsPrime(int);

//無痛 7-18

int main()
{
	printf("1~1000的質數:\n");
	for (int i = 1;i <= 1000;i++) {
		if (IsPrime(i)) printf("%d ", i);
	}
	system("pause");
    return 0;
}

bool IsPrime(int Num) {
	if (Num <= 1) return false;
	for (int i = 2; i * i <= Num; i++) {
		if (Num%i == 0) return false;
	}
	return true;

}
