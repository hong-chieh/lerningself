// Macro.cpp: 定義主控台應用程式的進入點。
//

#include "stdafx.h"
#define M_PI 3.14159265358979323846
#define M_DEG2RAD M_PI/180.0
#define M_RAD2DEG 180.0/M_PI
#define INPUT_ASK "想要弧度還是角度咧? 1.角度 2.弧度\n"
#define DEG_ASK "請輸入需轉換角度: "
#define RAD_ASK "請輸入需轉換弧度: "
#define DEG_OPTION 1
#define RAD_OPTION 2

//無痛 7-41

int main()
{
	int x;
	double y;
	printf(INPUT_ASK);
	scanf_s("%d", &x);
	if (x == DEG_OPTION) {
		printf(DEG_ASK);
		scanf_s("%lf", &y);
		y = y * M_DEG2RAD;
		printf("對應弧度為: %lf", y);
	}
	else if (x == RAD_OPTION) {
		printf(RAD_ASK);
		scanf_s("%lf", &y);
		y = y * M_RAD2DEG;
		printf("對應角度為: %lf\n", y);
	}
	system("pause");
    return 0;
}

